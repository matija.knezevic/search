<?php
defined('_JEXEC') or die;
$doc = JFactory::getDocument();

require_once dirname(__FILE__) . '/helper.php';

if ($params->def('prepare_content', 1))
{
	JPluginHelper::importPlugin('content');
	$module->content = JHtml::_('content.prepare', $module->content, '', 'mod_vozila.content');
}

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_vozila', $params->get('layout', 'default'));